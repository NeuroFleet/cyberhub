

>>> from pattern.vector import stem, PORTER, LEMMA
>>> 
>>> print stem('spies', stemmer=PORTER)
>>> print stem('spies', stemmer=LEMMA)
 
spi
spy
>>> from pattern.vector import count, words, PORTER, LEMMA
>>> 
>>> s = 'The black cat was spying on the white cat.'
>>> print count(words(s), stemmer=PORTER)
>>> print count(words(s), stemmer=LEMMA) 
 
{u'spi': 1, u'white': 1, u'black': 1, u'cat': 2}
>>> from pattern.vector import count, LEMMA
>>> from pattern.en import parse, Sentence
>>>  
>>> s = 'The black cat was spying on the white cat.'
>>> s = Sentence(parse(s))
>>> print count(s, stemmer=LEMMA)
 
{u'spy': 1, u'white': 1, u'black': 1, u'cat': 2, u'.': 1} 

>>> from pattern.vector import chngrams
>>> print chngrams('The cat sat on the mat.'.lower(), n=3)
 
{' ca': 1, 'at ': 2, 'he ': 2, 't o': 1, 
 ' ma': 1, 'at.': 1, 'mat': 1, 't s': 1, 
 ' on': 1, 'cat': 1, 'n t': 1, 'the': 2,
 ' sa': 1, 'e c': 1, 'on ': 1, 
 ' th': 1, 'e m': 1, 'sat': 1
}

>>> from pattern.vector import Document
>>> 
>>> s = '''
>>>     The shuttle Discovery, already delayed three times by technical problems 
>>>     and bad weather, was grounded again Friday, this time by a potentially 
>>>     dangerous gaseous hydrogen leak in a vent line attached to the shipʼs 
>>>     external tank. The Discovery was initially scheduled to make its 39th 
>>>     and final flight last Monday, bearing fresh supplies and an intelligent 
>>>     robot for the International Space Station. But complications delayed the 
>>>     flight from Monday to Friday,  when the hydrogen leak led NASA to conclude 
>>>     that the shuttle would not be ready to launch before its flight window 
>>>     closed this Monday.
>>> '''
>>> d = Document(s, threshold=1)
>>> print d.keywords(top=6)
 
[(0.17, u'flight'), 
 (0.17, u'monday'), 
 (0.11, u'delayed'), 
 (0.11, u'discovery'), 
 (0.11, u'friday'),
 (0.11, u'hydrogen')
]

>>> from pattern.vector import Document, Model, TFIDF
>>>  
>>> d1 = Document('A tiger is a big yellow cat with stripes.', type='tiger')
>>> d2 = Document('A lion is a big yellow cat with manes.', type='lion',)
>>> d3 = Document('An elephant is a big grey animal with a slurf.', type='elephant')
>>>  
>>> print d1.vector
>>>
>>> m = Model(documents=[d1, d2, d3], weight=TFIDF)
>>>  
>>> print d1.vector
>>> print
>>> print m.similarity(d1, d2) # tiger vs. lion
>>> print m.similarity(d1, d3) # tiger vs. elephant
 
{u'tiger': 0.25, u'stripes': 0.25, u'yellow': 0.25, u'cat': 0.25} # TF
{u'tiger': 0.27, u'stripes': 0.27, u'yellow': 0.10, u'cat': 0.10} # TFIDF
 
0.12
0.0

>>> from pattern.vector import Document, Model
>>>
>>> d1 = Document('The cat purrs.', name='cat1')
>>> d2 = Document('Curiosity killed the cat.', name='cat2')
>>> d3 = Document('The dog wags his tail.', name='dog1')
>>> d4 = Document('The dog is happy.', name='dog2')
>>> 
>>> m = Model([d1, d2, d3, d4])
>>> m.reduce(2)
>>> 
>>> for d in m.documents:
>>>     print
>>>     print d.name
>>>     for concept, w1 in m.lsa.vectors[d.id].items():
>>>         for feature, w2 in m.lsa.concepts[concept].items():
>>>             if w1 != 0 and w2 != 0:
>>>                 print (feature, w1 * w2)

>>> from pattern.vector import Document, Model, HIERARCHICAL
>>>  
>>> d1 = Document('Cats are independent pets.', name='cat')
>>> d2 = Document('Dogs are trustworthy pets.', name='dog')
>>> d3 = Document('Boxes are made of cardboard.', name='box')
>>>  
>>> m = Model((d1, d2, d3))
>>> print m.cluster(method=HIERARCHICAL, k=2)
 
Cluster([
    Document(id=3, name='box'), 
    Cluster([
        Document(id=2, name='dog'), 
        Document(id=1, name='cat')
    ])
])


