# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class JumiaSpider(CrawlSpider):
    name = "jumia"
    allowed_domains = [
        "jumia.ma",
        "www.jumia.ma",
    ]
    
    start_urls = (
        'http://www.jumia.ma/',
    )
    
    rules = (
        Rule(LinkExtractor(allow=('(.*)', )),    callback='parse_page',   follow=True),
    )
    
    #######################################################################################################################
    
    def parse_page(self, response):
        if len(response.xpath('//body[contains(@class,"l-prd-detail")]')):
            entry = RetailArticle()
            
            entry['site']            = self.name
            entry['link']            = response.url
            
            entry['specs']           = {
                'brand': {
                    'name': cleanup(response.xpath('//a[@class="brand-link"]/text()'), sep=None, trim=True),
                    'link': cleanup(response.xpath('//a[@class="brand-link"]/@href'), sep=None, trim=True),
                },
                'technical':  dict([
                    set([
                        x.replace('\u200e','').strip()
                        for x in cleanup(ln.xpath('./text()'), sep=None, trim=True).split(':',1)
                    ])
                    for ln in response.xpath('//div[contains(@class,"usp-shortdescr")]//li')
                ]),
            }
            
            for tr in response.xpath('//table[contains(@class,"prd-attributes")]//tr'):
                key = cleanup(tr.xpath('./th/text()'), sep=None, trim=True)
                
                entry['specs'][key.lower()] = cleanup(tr.xpath('./td/text()'), sep=None, trim=True)
            
            entry['title']           = cleanup(response.xpath('//*[contains(@class,"prd-title")]/text()'), sep=None, trim=True)
            entry['description']     = cleanup(response.xpath('//div[contains(@class,"prd-description")]//text()'), sep='\n', trim=True)
            
            entry['pricing']         = dict(
                regular = {
                    'value':  cleanup(response.xpath('//span[@id="price_box"][1]//*/@data-price'), sep=None, trim=True),
                    'devise': cleanup(response.xpath('//span[@id="price_box"][1]//*/@data-currency-iso'), sep=None, trim=True),
                },
                special = {
                    'value':  cleanup(response.xpath('//span[@id="price_box"][1]//*/@data-price'), sep=None, trim=True),
                    'devise': cleanup(response.xpath('//span[@id="price_box"][1]//*/@data-currency-iso'), sep=None, trim=True),
                },
            )
            
            entry['media'] = {
                'links': response.xpath('//div[@class="prd-moreImages"]//li[@data-image-product]/@data-image-product').extract(),
            }
            
            if 'sku' in entry['specs']:
                entry['uid'] = entry['specs']['sku']
                
                del entry['specs']['sku']
                
                yield entry

