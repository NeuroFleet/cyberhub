#!/bin/bash

cd /stack

git pull

git checkout master

if [[ ! -d /stack/logs ]] ; then
    apt update ; apt -y --force-yes upgrade

    apt install -y --force-yes linux-{image,headers}-generic-lts-xenial \
          {kernel,io,if,h,virt-}top screen traceroute nmap tree un{zip,rar} \
          zsh zsh-lovers zsh-antigen docker.io virtualbox-{dkms,ext-pack} \
          zlib1g-dev uuid-dev libmnl-dev gcc make git autoconf autoconf-archive \
          autogen automake pkg-config curl build-essential python-pip nodejs cython \
          supervisor nginx-full apache2 libapache2-mod-php libapache2-mod-php7.0 \
          php7.0-{bz2,cli,curl,dev,fpm,gd,imap,json,ldap,mbstring,mcrypt,mysql,pgsql,readline,recode,soap,sqlite3,tidy,xml,xmlrpc,xsl}

    ufw allow in 22/tcp
    ufw allow in 80/tcp
    ufw allow in 443/tcp

    cp -afR /stack/config/supervisor.conf     /etc/supervisor/supervisor.conf
    cp -afR /stack/config/apache2-ports.conf  /etc/apache2/ports.conf

    cp -afR /stack/config/nginx.d/*.conf      /etc/nginx/sites-enabled/
    cp -afR /stack/config/apache.d/*.conf     /etc/apache2/sites-enabled/
    cp -afR /stack/config/supervisor.d/*.conf /etc/supervisor/conf.d/

    rm -f /etc/apache2/mods-enabled/mpm_event.*
    ln -sf /etc/apache2/mods-available/mpm_prefork.* /etc/apache2/mods-enabled/

    mkdir /stack/logs
fi

#*******************************************************************************

if [[ ! -d /opt/netdata ]] ; then
    cd /opt

    git clone https://github.com/firehol/netdata.git --depth=1

    cd netdata

    ./netdata-installer.sh
fi

################################################################################

if [[ ! -d /var/www/virt ]] ; then
    cd /var/www

    wget https://freefr.dl.sourceforge.net/project/phpvirtualbox/phpvirtualbox-5.0-5.zip

    unzip phpvirtualbox-5.0-5.zip

    rm -f phpvirtualbox-5.0-5.zip

    mv phpvirtualbox-5.0-5 virt

    chown www-data:www-data -R virt/

    VBoxManage setproperty vrdeauthlibrary null
    VBoxManage setproperty websrvauthlibrary null
fi

#*******************************************************************************

if [[ ! -d /stack/iso ]] ; then
    mkdir -p /stack/iso/{windows,linux}
fi

#*******************************************************************************

cd /stack/iso/linux

export ISO_LIST="`echo http://releases.ubuntu.com/16.04/ubuntu-16.04.2-{desktop,server}-{i386,amd64}.iso`"
export ISO_LIST="$ISO_LIST `echo http://cdimage.kali.org/kali-2016.2/kali-linux-2016.2-{i386,amd64}.iso`"

for img in `echo $ISO_LIST` ; do
    wget -c $img
done

#*******************************************************************************

cd /stack/iso/windows

export ISO_LIST="http://emailhostingsupport.com/pcriver.com_server_2012_r2_essentials.iso"
export ISO_LIST="$ISO_LIST `echo http://emailhostingsupport.com/pcriver.com_Windows_XP_{Pro_SP3_32_,Professional_64-}bit.iso`"
export ISO_LIST="$ISO_LIST `echo http://s2.softlay.net/files/en_windows_7_ultimate_x64_dvd.iso?st=ORsW01V2LYWU1-aibl21DA&e=1487452585`"
export ISO_LIST="$ISO_LIST `echo http://emailhostingsupport.com/pcriver.com_windows_10_pro_{32,64}_bit.iso`"

for img in `echo $ISO_LIST` ; do
    wget -c $img
done

################################################################################

if [[ ! -d /stack/virt ]] ; then
    WARRIOR_VAGRANT="scotch/box cloudfoundry/bosh-lite data-science-toolbox/dst roots/bedrock"

    WARRIOR_VAGRANT=$WARRIOR_VAGRANT" "`echo ubuntu/{precise,trusty,vivid,wily}{32,64}`
    WARRIOR_VAGRANT=$WARRIOR_VAGRANT" "`echo bento/{centos-7.2,fedora-23,debian-8.2,ubuntu-{12,14,15}.04,opensuse-13.2,freebsd-9.3}`

    for system in `echo $OS_LIST` ; do
	    WARRIOR_VAGRANT="$WARRIOR_VAGRANT os2use/$system"
    done
fi

#*******************************************************************************

docker run --name swarm -p 9000:39999 --env ADMIN_USERNAME=rwich --env ADMIN_PASS=moncif --privileged -v /var/run/docker.sock:/var/run/docker.sock --restart always -d portainer/portainer

