#!/bin/bash

PKGs="kalilinux/kali-linux-docker"


#PKGs=$PKGs" "`echo busybox ubuntu`

PKGs=$PKGs" "`echo daemon2use/{traccar,asterisk,collectd}`
PKGs=$PKGs" "`echo nginx:latest rabbitmq:latest `

PKGs=$PKGs" "`echo redis:3.2 mongo:3.3 neo4j:3.0`
PKGs=$PKGs" "`echo mysql:5.7 postgres:9.6 solr:5.3`
PKGs=$PKGs" "`echo {elasticsearch,cassandra,rethinkdb,aerospike}:latest couchbase:communityt`
PKGs=$PKGs" "`echo tenforce/virtuoso stain/jena-fuseki nicholsn/docker-marmotta`
PKGs=$PKGs" "`echo sequenceiq/{hadoop-docker:2.7.1,spark:1.6.0}`
PKGs=$PKGs" "`echo backend2use/{rexter-titan,gaffer}`

PKGs=$PKGs" "`echo openkm:8.0 odoo:9.0 mcsaky/openkm:latest`

PKGs=$PKGs" "`echo registry swarm`
PKGs=$PKGs" "`echo kibana logstash sentry`
PKGs=$PKGs" "`echo jenkins redmine sonarqube`

for key in `echo $PKGs` ; do
	gnome-terminal -e "docker pull $key" -t "Docker :: "$key &
done
