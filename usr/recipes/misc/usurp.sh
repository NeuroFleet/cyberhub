#!/bin/bash

clear

TARGET=$HOME

##############################################################################

sudo apt-get update

sudo apt-get install -y --force-yes apt-cacher-ng git

COMMON=/media/ubuntu/VAULT/Deveption/jetpack

for key in bind apt  ; do
    sudo cp -fR $COMMON/$key /etc/
done

SOURCE=/media/ubuntu/VAULT/Deveption/roaming

for key in .ssh  ; do
    cp -afR $SOURCE/$key $TARGET/
done

##############################################################################

sudo chown apt-cacher-ng:apt-cacher-ng -R /media/ubuntu/THEQUE/Systema/Cache/Apt

sudo mount --bind /media/ubuntu/THEQUE/Systema/Cache/Apt /var/cache/apt-cacher-ng

sudo service apt-cacher-ng restart

sudo apt-get update

PKGs="htop git build-essential cython"
PKGs=`echo $PKGs python-{virtualenv,pip} php5-cli ruby`
PKGs=`echo $PKGs vlc gmpc gstreamer0.10-{libav,fluendo-mp3,plugins-{ugly,bad-{faad,videoparsers}}}`

sudo apt-get install -y --force-yes `echo $PKGs`

sudo pip install -U httpie

sudo gem --no-rdoc --no-ri foreman
