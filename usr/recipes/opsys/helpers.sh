#!/bin/bash

#############################################################################

get_repo () {
    gnome-terminal -e $HOME"/opsys/ensure.sh "$1" "$2 -t "Updating "$1 &
}

#****************************************************************************

get_meta () {
    VHOST_TYPE=$1
    VHOST_NAME=$2

    VHOST_ROOT=/srv/meta/$VHOST_TYPE/$VHOST_NAME

    get_repo bitbucket.org:meta-$VHOST_TYPE/$VHOST_NAME.git $VHOST_ROOT
}

#****************************************************************************

get_rhc () {
    if [[ ! -d $HOME/devel/rhcloud/$2/$3 ]] ; then
        git clone ssh://$3@$2-$1.rhcloud.com/~/git/$2.git/ $HOME/devel/rhcloud/$1/$2 --recursive
    fi
}

###################################################################################################

get_contain () {
    for key in $4 ; do
        echo "*) "$key" :"
    done

    for key in $LST ; do
	TARGET=$1/$key

        if [[ ! -d $TARGET ]] ; then
            git clone git@$2/$key.git $TARGET --recursive
        fi

        docker pull $3/$key
    done
}

#****************************************************************************

ensure_git () {
    if [[ ! -d $2 ]] ; then
        git clone git@$1 $2
    fi

    if [[ -d $2 ]] ; then
        cd $2

        git pull -u origin master

        git submodule update --init --recursive

        git submodule foreach git pull origin master

        if [[ -f composer.json ]] ; then
            if [[ ! -d vendor ]] ; then
                composer install
            else
                composer update
            fi
        fi

        if [[ -f package.json ]] ; then
            npm install
        fi

        if [[ -f requirements.txt ]] ; then
            if [[ ! -d venv ]] ; then
                virtualenv --distribute venv

                venv/bin/easy_install -U pip
            fi

            if [[ -d venv ]] ; then
                venv/bin/pip install -r requirements.txt
            fi
        fi
    fi
}
