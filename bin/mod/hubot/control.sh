#!/bin/bash

if [[ "x$RONIN_PERSON" == "x" ]] ; then echo "Ghost person not defined. Try : export RONIN_PERSON=proto" ; return ; fi

export RONIN_TARGET=$HUBOT_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9600 ; fi

################################################################################

sysV_ctrl () {
    case $1 in
        clean)
            ;;
        build)
            ;;
        release)
            python -m gestalt.cmd.compile
            ;;
        ########################################################################
        shell)
            bash -i
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

