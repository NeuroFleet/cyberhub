#!/bin/bash

for key in hubot external vtr ; do
    symlink $RONIN_SRV/$key-scripts.json $HUBOT_HOME/$key-scripts.json
done

ln -sf $RONIN_LIB/python/$RONIN_REACTOR/converse/*.coffee    $HUBOT_HOME/scripts/

ln -sf $RONIN_LIB/python/$RONIN_NARROW/*/converse/*.coffee $HUBOT_HOME/scripts/

