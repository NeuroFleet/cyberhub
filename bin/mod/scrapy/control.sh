#!/bin/bash

if [[ "x$RONIN_PERSON" == "x" ]] ; then echo "Ghost person not defined. Try : export RONIN_PERSON=proto" ; return ; fi

export RONIN_TARGET=$SCRAPY_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9300 ; fi

################################################################################

sysV_ctrl () {
    case $1 in
        clean)
            ;;
        build)
            ;;
        release)
            ;;
        ########################################################################
        shell)
            ipython
            ;;
        invite)
            cd $SCRAPY_HOME

            runner scrapy shell -i bpython
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

