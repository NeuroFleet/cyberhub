@cli.command('router')
@click.option('--root', default='wsgi') #, choices=['http','wsgi','html'])
#@click.option('--rq_workers', type=int, default=0)
@click.option('--rq', multiple=True)
@click.option('--tpf/--no-tpf', default=False)
@click.option('--ldp/--no-ldp', default=False)
@click.option('--job/--no-job', default=True)
@click.pass_context
def run_hub(ctx, *args, **kwargs):
    reg = {
        'serve':   [],
        'routing': {},
    } ; reg.update(ctx.obj['cross']['parts'])

    for key in ('paths','hooks','socks'):
        reg['routing'].update(reg[key])

    #*********************************************************************

    if kwargs['root']=='wsgi':
        reg['routing']["/"] = {
            "type": "wsgi", "object": "app",
            "module": "reflector.wsgi",
        }
    elif kwargs['root']=='html':
        reg['routing']["/"] = {
            "type": "static",
            "directory": "../serve/content",
            "options": { "enable_directory_listing": True }
        }
    else: # elif kwargs['root']=='http':
        reg['serve'] += ctx.obj['cross']['worker'](
            prog='./control', args=["server","--port",ctx.obj['port']['wsgi']],
            wdir="../", spwn=["../amber","../daten"],
        )

        reg["routing"]["/"] = {
            "type": "reverseproxy",
            "host": "localhost",
            "port": ctx.obj['port']['wsgi'],
            "path": "/"
        }

    #*********************************************************************

    ind = 1

    for queue in kwargs['rq']:
        params = []

        for key in queue.split(','):
            params += ["-q", key]

        reg['serve'] += ctx.obj['cross']['worker'](
            prog='./control', args=["-n","internal-%d" % ind] + params,
            wdir="../", spwn=["../codes","../daten"],
        )

        ind += 1

    #if kwargs['rq_dash']:
    #    reg['serve'] += ctx.obj['cross']['worker'](
    #        prog='rq-dashboard', args=[
    #            "-p", str(ctx.obj['port']['rq']),
    #            " --url-prefix", "/queues",
    #        ],
    #        wdir="../", spwn=["../codes","../daten"],
    #    )
    #
    #    reg['paths']['queues'] = {
    #        "type": "reverseproxy",
    #        "host": "localhost",
    #        "port": ctx.obj['port']['rq'],
    #        "path": "/"
    #    }

    #*********************************************************************

    if kwargs['job']:
        reg['serve'] += ctx.obj['cross']['worker'](
            prog='supervisord', args=["-c","amber/daemon.conf", "-n"],
            wdir="../", spwn=["../amber/service.d"],
        )

        reg['paths']['job'] = {
            "type": "reverseproxy",
            "host": "localhost",
            "port": ctx.obj['port']['job'],
            "path": "/job"
        }

    if kwargs['tpf']:
        reg['serve'] += ctx.obj['cross']['worker'](
            prog='ldf-server', args=["config.json","7011", "2"],
            wdir="../daten/tpf", spwn=["../daten/tpf"],
        )

        reg['paths']['tpf'] = {
            "type": "reverseproxy",
            "host": "localhost",
            "port": ctx.obj['port']['tpf'],
            "path": "/tpf"
        }

    if kwargs['ldp']:
        reg['serve'] += ctx.obj['cross']['worker'](
            prog='solid', args=[
                "start", "--port", "7012", "--no-webid",
                "--mount", "/ldp", "--root", "."
            ],
            wdir="../daten/ldp", spwn=["../daten/ldp"],
        )

        reg['paths']['ldp'] = {
            "type": "reverseproxy",
            "host": "localhost",
            "port": ctx.obj['port']['ldp'],
            "path": "/ldp"
        }

    #*********************************************************************

    reg['content'] = {
        "version" : 2,
        "controller" : {
        },
        "workers" : [{
            "type" : "router",
            "options": {
                "title": "Neuro::Psycho",
                "reactor": {
                    "linux": "epoll",
                },
                #"cpu_affinity": [1, 1],
                "env": {
                    #"GHOST_MODULE": "crossbar",
                },
                "pythonpath": ["../codes/python"]
            },
            "transports" : [{
                "type" : "websocket",
                "endpoint" : {
                    "type" : "tcp",
                    "port" : ctx.obj['port']['sock']
                }
            },{
                "type" : "web",
                "endpoint" : {
                    "type" : "tcp",
                    "port" : ctx.obj['port']['http']
                },
                "paths" : reg['routing'],
            },{
                "type": "flashpolicy",
                "allowed_domain": "*",
                "allowed_ports": [
                    ctx.obj['port']['flsh']
                ],
                "endpoint": {
                    "type": "tcp",
                    "port": ctx.obj['port']['flsh']
                }
            }],
            "realms" : reg['realm'],
            "components" : reg['comps'],
        }] + reg['serve'] + reg['progs']
    }

    with open(rpath('amber','config.json'), 'w+') as f:
        f.write(json.dumps(reg['content']))

    os.system('crossbar start --cbdir amber/')

#***************************************************************************************

@cli.command('server')
@click.pass_context
def run_web(ctx, *args, **kwargs):
    from reflector.wsgi import app

    print "Running WSGI application at http://localhost:%(http)d/" % ctx.obj['port']

    app.run(port=int(ctx.obj['port']['http']))

