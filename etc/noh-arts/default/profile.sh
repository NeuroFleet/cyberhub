#!/bin/bash

#export REDIS_URL="redis://localhost:6397/0"
#export MONGODB_URL="mongodb://localhost/prism"
#export NEO4J_URL="http://localhost:7474"

#export NLTK_CORP=`echo book gutenburg {prop,tree}bank {verb,word}net udhr{,2}`
##export NLTK_CORP=`echo conll200{0,2,7} city_database dependency_treebank dolch framenet_v1{5,7} genesis knbc names omw pil propbank pros_cons ptb reuters semcor senseval sentence_polarity sentiwordnet state_union shakespeare subjectivity switchboard toolbox words webtext`
export NLTK_CORP=( `echo book {prop,{,dependency_}tree}bank {name,verb,word}s {verb,{,senti}word}net udhr{,2} conll200{0,2,7} framenet_v1{5,7} city_database dolch genesis gutenburg knbc omw pil pros_cons ptb reuters semcor senseval sentence_polarity state_union shakespeare subjectivity switchboard toolbox webtext` )

