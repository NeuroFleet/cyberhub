#!/usr/bin/env python

from ronin.provision.shortcuts import *

########################################################################################

script = open('%s/etc/provision.py' % ROOT_PATH).read()

exec script in globals(),locals()

#***************************************************************************************

@click.group()
@click.option('--path', default=None)
@click.pass_context
def cli(ctx, *args, **kwargs):
    ctx.obj.update({
        'rpath': kwargs['path'],
        'cross': instantiate(**kwargs),
    })

#***************************************************************************************

def decorate_init(cmd):
    for x in commandline(cli, cmd):
        cmd = x(cmd)

    return cmd

########################################################################################

noh_arts = os.environ['NOH_ARTs'].strip().split(' ')
noh_maps = {}

#***************************************************************************************

for key in os.environ.keys():
    nrw = key.split('_')

    if len(nrw)==4:
        if (nrw[0]=='NOH') and (nrw[1]=='ART'):
            if nrw[2] in noh_arts:
                if nrw[2] not in noh_maps:
                    noh_maps[nrw[2]] = {
                        'flag': nrw[2],
                        'name': nrw[2].lower(),
                    }

                if nrw[3] in ('NAME','PATH','TYPE','LINK'):
                    field = dict(
                        NAME='text',
                    ).get(nrw[3],nrw[3].lower())

                    noh_maps[nrw[2]][field] = os.environ[key]

#***************************************************************************************

#print "Noh-Arts :> ", noh_arts, noh_maps

########################################################################################

def noh_manager(hub, art):
    return [art]

#***************************************************************************************

def noh_wrap(hub,hnd):
    for art in noh_arts:
        if art in noh_maps:
            cfg = noh_maps[art]

            yield cfg['text'], hnd(hub, cfg)

########################################################################################

@cli.command('book')
@click.pass_context
def package_managers(ctx, *args, **kwargs):
    for keyname,listing in noh_wrap(ctx.obj['cross'], noh_manager):
        print "*) %s :" % keyname.capitalize()

        for art in listing:
            print "\t-> name: %(name)s" % art

            for key,value in [
                (k,v)
                for k,v in art.iteritems()
                if v and (k not in ['name','flag'])
            ]:
                print "\t   %s: %s" % (key, value)

            print "\t   list: [%s]" % ', '.join([
                pth for pth in os.listdir(art['path'])
                if os.path.isdir(os.path.join(art['path'], pth))
            ])

########################################################################################

@cli.command('list')
@click.pass_context
def package_list(ctx, *args, **kwargs):
    ctx.obj['cross'].execute('git','pull','-u','origin','master')

#***************************************************************************************

@cli.command('find')
@click.argument('packages', nargs='*')
@click.pass_context
def package_find(ctx, *args, **kwargs):
    ctx.obj['cross'].execute('git','commit','-a','-m',"Debugging ...")

#***************************************************************************************

@cli.command('show')
@click.argument('packages', nargs='*')
@click.pass_context
def package_show(ctx, *args, **kwargs):
    ctx.obj['cross'].execute('git','push','-u','origin','master')

#***************************************************************************************

@cli.command('deps')
@click.argument('packages', nargs='*')
@click.pass_context
def package_deps(ctx, *args, **kwargs):
    ctx.obj['cross'].execute('git','remote','-v')

########################################################################################

@cli.command('install')
@click.argument('packages', nargs='*')
@click.pass_context
def package_install(ctx, *args, **kwargs):
    ctx.obj['cross'].log('INFO', "Started provisioning the reactor ...")

    provision(ctx.obj['cross'], **kwargs)

    ctx.obj['cross'].log('INFO', "Finishing provisioning the reactor ...")

    ctx.obj['cross'].save()

    ctx.obj['cross'].log('INFO', "Reactor provisionned at : %s" % ctx.obj['cross'].bpath)

#***************************************************************************************

@cli.command('remove')
@click.argument('packages', nargs='*')
@click.pass_context
def package_remove(ctx, *packages, **kwargs):
    ctx.obj['cross'].log('INFO', "Started provisioning the reactor ...")

    provision(ctx.obj['cross'], **kwargs)

    ctx.obj['cross'].log('INFO', "Finishing provisioning the reactor ...")

    ctx.obj['cross'].save()

    ctx.obj['cross'].log('INFO', "Reactor provisionned at : %s" % ctx.obj['cross'].bpath)

########################################################################################

@cli.command('load')
@click.pass_context
def package_load(ctx, *args, **kwargs):
    ctx.obj['cross'].execute('git','pull','-u','origin','master')

#***************************************************************************************

@cli.command('dump')
@click.pass_context
def package_dump(ctx, *args, **kwargs):
    ctx.obj['cross'].execute('git','push','-u','origin','master')

########################################################################################

@cli.command('grub')
#@click.option('--count', default=1, help='Number of greetings.')
#@click.option('--skin', prompt=True, default=lambda: os.environ.get('GHOST_SKIN', 'color-admin,control-frog'))
@click.pass_context
def grub2_setup(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    ctx.obj.produce(ctx.obj.rpath('boot','grub','menu.ipxe'),
        'ipxe.cfg',
    **ctx.obj.context)

    for key in (['fonts'],['themes',ctx.obj.config['theme']]):
        if not os.path.exists(ctx.obj.rpath('boot','grub',*key)):
            ctx.obj('cp', '-afR', ctx.obj.rpath('usr','share','grub2',*key), ctx.obj.rpath('boot','grub',*key))

    ctx.obj.produce(ctx.obj.rpath('boot','grub','grub.cfg'),
        'grub.cfg',
    **ctx.obj.context)

    for OS in ctx.obj.systems.values():
        tpl = ['os/%s/main.cfg' % OS.alias]

        if 'based_on' in OS.config:
            tpl += ['os/%s/main.cfg' % OS.config['based_on']]

        ctx.obj.produce(ctx.obj.rpath('boot','grub','conf.d','distro-%s.cfg' % OS.alias), *tpl, **ctx.obj.context)

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
        }
    )

