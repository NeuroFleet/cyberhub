#!/usr/bin/env python

from ronin.provision.shortcuts import *

########################################################################################

script = open('%s/etc/provision.py' % ROOT_PATH).read()

exec script in globals(),locals()

#***************************************************************************************

@click.group()
@click.option('--path', default=None)
@click.pass_context
def cli(ctx, *args, **kwargs):
    ctx.obj.update({
        'rpath': kwargs['path'],
        'cross': instantiate(**kwargs),
    })

#***************************************************************************************

def decorate_init(cmd):
    for x in commandline(cli, cmd):
        cmd = x(cmd)

    return cmd

#***************************************************************************************

#@click.option('--name', prompt='Your name please')
#@click.option('--username', prompt=True, default=lambda: os.environ.get('USER', ''))
#@click.option('--password', prompt=True, hide_input=True, confirmation_prompt=True)
@cli.command('setup')
@decorate_init
@click.pass_context
def initialize(ctx, *args, **kwargs):
    ctx.obj['cross'].log('INFO', "Started provisioning the reactor ...")

    provision(ctx.obj['cross'], **kwargs)

    ctx.obj['cross'].log('INFO', "Finishing provisioning the reactor ...")

    ctx.obj['cross'].save()

    ctx.obj['cross'].log('INFO', "Reactor provisionned at : %s" % ctx.obj['cross'].bpath)

########################################################################################

def git_find_repo(hub, prefix):
    result = {}

    for key in os.listdir(hub.rpath(prefix)):
        if os.path.exists(hub.rpath(prefix,key,'.git')):
            result[key] = hub.rpath(prefix,key)

    return result

#***************************************************************************************

def git_list_repo(hub):
    yield 'system',   git_find_repo(hub, 'sys')
    yield 'language', git_find_repo(hub, 'lib')
    yield 'feature',  git_find_repo(hub, 'dev')

    yield 'domain',   git_find_repo(hub, 'opt')
    yield 'organism', git_find_repo(hub, 'home')

########################################################################################

@cli.command('list')
@click.pass_context
def git_repos(ctx, *args, **kwargs):
    for keyname,listing in git_list_repo(ctx.obj['cross']):
        print "*) %s :" % keyname.capitalize()

        for name,path in listing.iteritems():
            print "\t-> name: %s" % name
            print "\t   path: %s" % path

#***************************************************************************************

@cli.command('status')
@click.pass_context
def git_status(ctx, *args, **kwargs):
    for keyname,listing in git_list_repo(ctx.obj['cross']):
        print "*) %s :" % keyname.capitalize()

        for name,path in listing.iteritems():
            proxy = git.Repo(path)

            if proxy.is_dirty():
                print "\t-> name: %s" % name
                print "\t   path: %s" % path

########################################################################################
########################################################################################
########################################################################################

def elf_detect_dir(hub, prefix, key):
    if os.path.exists(hub.rpath(prefix,key,'.git')):
        return hub.rpath(prefix,key)

def elf_detect_git(hub, prefix, key):
    if os.path.exists(hub.rpath(prefix,key,'.git')):
        return git.Repo(hub.rpath(prefix,key))

def elf_detect_spec(hub, prefix, key, part):
    resp = {}

    if os.path.isdir(hub.rpath(prefix,key,'spec',part)):
        for item in os.listdir(hub.rpath(prefix,key,'spec',part)):
            if os.path.isdir(hub.rpath(prefix,key,'spec',part,item)):
                entry = {
                    'verse': prefix,
                    'realm': key,

                    'type': part,
                    'name': item,
                    'path': hub.rpath(prefix,key,'spec',part,item),
                    'line': ['http','https','ws','wss'],
                    'host': 'schema.%s' % hub.domain,
                }

                entry['addr'] = {
                    'sys': 'bunraku.%(realm)s.%(name)s',
                    'lib': 'oshiguma.%(realm)s.%(name)s',
                    'dev': 'kabuki.%(realm)s.%(name)s',
                    'opt': 'vernam.%(realm)s.%(name)s',
                }.get(entry['verse'], '%(verse)s/%(realm)s/%(name)s') % entry

                entry['base'] = '/%(type)s/%(addr)s' % entry

                entry['link'] = [
                    ('%s://' % scheme) + ('%(host)s%(base)s' % entry)
                    for scheme in entry['line']
                ]

                resp[entry['name']] = entry

    if len(resp):
        return resp

#***************************************************************************************

def elf_do_find(hub, prefix, callback, *args, **kwargs):
    result = {}

    for key in os.listdir(hub.rpath(prefix)):
        if os.path.isdir(hub.rpath(prefix,key)):
            value = callback(hub, prefix, key, *args, **kwargs)

            if value is not None:
                result[key] = value

    return result

#***************************************************************************************

def elf_do_list(hub, callback, *args, **kwargs):
    yield 'system',   elf_do_find(hub, 'sys',  callback, *args, **kwargs)
    yield 'language', elf_do_find(hub, 'lib',  callback, *args, **kwargs)
    yield 'feature',  elf_do_find(hub, 'dev',  callback, *args, **kwargs)

    yield 'domain',   elf_do_find(hub, 'opt',  callback, *args, **kwargs)
    yield 'organism', elf_do_find(hub, 'home', callback, *args, **kwargs)

########################################################################################

@cli.command('blueprints')
@click.pass_context
def elf_swagger(ctx, *args, **kwargs):
    for keyname,listing in elf_do_list(ctx.obj['cross'], elf_detect_spec, 'ontology'):
        print "*) %s :" % keyname.capitalize()

        for sublist in listing.values():
            for name,entry in sublist.iteritems():
                for word in ('meta','schema','data'):
                    target = os.path.join(entry['path'], '%s.swagger' % word)

                    if os.path.exists(target):
                        entry[word] = ctx.obj['cross'].read_yaml(target)

                print "\t-> name:\t%s" % name
                print "\t   mode:\t%s" % entry['realm']

                for field,value in entry.iteritems():
                    if field not in (
                        'realm','verse','path','type','name',
                        'line','host','base','link',
                        'meta','schema','data',
                    ):
                        print "\t   %s:\t%s" % (field,value)

                if len(entry['link']):
                    print "\t   link:\t%s" % entry['link'][0]

                    #print "\t   link:"
                    #for link in entry.get('link', []):
                    #    print "\t       - %s" % link

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
        }
    )

