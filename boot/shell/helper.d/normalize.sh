export AIRFLOW_URL=http://pipes.$UCHIKOMA_DOMAIN

export CROSSBAR_URL=http://pipes.$UCHIKOMA_DOMAIN

#****************************************************************************

export LUIGI_SCHEDULER_URL=$CROSSBAR_URL/luigi
export LUIGI_SCHEDULER_PORT=9101

#*******************************************************************************

dock_all () {
    if [[ ! -d /stack/data ]] ; then
        mkdir -p /stack/data/{business/{docs,drive,erp/addons},backends/{cache/redis,mongo,graph}}

        pip install docker-compose

        cd /stack
    fi

    cd $APP_ROOT

    docker-compose pull

    #docker-compose up
}

