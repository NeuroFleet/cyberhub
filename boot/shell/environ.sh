#!/bin/bash

export_dyn () {
    export $1=$2
}

export_default () {
    local value=$(eval_dyn $1)

    if [[ "x"$value == "x" ]] ; then
        export $1=$2
    fi
}

#*****************************************************************************************************

eval_dyn () {
    eval "echo \$${1}"
}

ronin_dyn_compile () {
    eval "echo \$${1}"
}

ronin_dyn_execute () {
    env 1>/dev/null 2>/dev/null - $1=$2
}

ronin_dyn_render () {
    echo $* | $RONIN_ROOT/sbin/render dyna
}

#*****************************************************************************************************

source_deep () {
    for path in $* ; do
        if [[ -d $path ]] ; then
            for target in `ls $path/*.sh` ; do
                source $target
            done
        fi
    done
}

source_with () {
    RONIN_EXTs=( $RONIN_SYSDIR )

    for lang in ${RONIN_LANGUAGEs[@]} ; do
        RONIN_EXTs=( "${RONIN_EXTs[@]}" $RONIN_ROOT/lib/$lang )
    done

    for realm in ${RONIN_REALMs[@]} ; do
        RONIN_EXTs=( "${RONIN_EXTs[@]}" $RONIN_ROOT/srv/$realm )
    done

    for path in ${RONIN_EXTs[@]} ; do
        for ext in sh $RONIN_SHELL_INVITE ; do
            target=$path/$1.$ext

            if [[ -f $target ]] ; then
                source $target
            fi
        done
    done
}

#*****************************************************************************************************

symlink () {
    if [[ ! -d $2 ]] ; then
        if [[ ! -f $2 ]] ; then
            ln -sf $1 $2
        fi
    fi
}

#*****************************************************************************************************

ensure_repo () {
    if [[ ! -d $3 ]] ; then
        git clone $4 $3 --recursive
    else
        echo $1" '$2' already exists at : "$3
    fi
}

