#!/bin/bash

################################################################################

#export RONIN_BOOT=$RONIN_ROOT/boot
#export RONIN_CODE=$RONIN_ROOT/lib

#source $RONIN_BOOT/kernel/load.sh
#source $RONIN_BOOT/kernel/wrap.sh
#source $RONIN_BOOT/kernel/core.sh
#source $RONIN_BOOT/kernel/spec.sh
#source $RONIN_BOOT/kernel/loop.sh

################################################################################

#source $RONIN_BOOT/noh/arts.sh

#*******************************************************************************

noh_register "ORG"   organism $RONIN_ROOT/home none
#noh_register "DATA"  daten    $RONIN_ROOT/mnt  none

noh_register "SYS"   system   $RONIN_ROOT/sys  git  "IT-NohArt"
noh_register "LANG"  language $RONIN_ROOT/lib  git  "IT-Kabuki"
noh_register "REALM" realm    $RONIN_ROOT/srv  git  "IT-Bunraku"
noh_register "FEAT"  feature  $RONIN_ROOT/dev  git  "IT-Kyogen"

################################################################################

#export OLD_PARTs=$RONIN_PARTs

#export RONIN_PARTs=`echo $OLD_PARTs`

#unset OLD_PARTs

#*******************************************************************************

for key in ${RONIN_PARTs[@]} ; do
    source $RONIN_BOOT/kernel/verse/$key.sh
done

#*******************************************************************************

motd_begin

#*******************************************************************************

for key in ${RONIN_PARTs[@]} ; do
    ronin_require_$key
done

#*******************************************************************************

for key in ${RONIN_PARTs[@]} ; do
    ronin_include_$key
done

#*******************************************************************************

motd_end

################################################################################

export PATH="$RONIN_PATH:$PATH"

################################################################################

KEYWORDs=( "hdd key nrw pth usr lang deps path rpath folder target script" )
KEYWORDs=( "${KEYWORDs[@]}" "TARGET_KEY" "TARGET_DIR" "TARGET_FOLDER" )

case $RONIN_MODE in
    install)
        for key in ${RONIN_PARTs[@]} ${RONIN_LANGUAGEs[@]} ; do
            KEYWORDs=`echo $KEYWORDs ronin_{require,include}_$key`
        done
        ;;
    *)
        KEYWORDs=`echo $KEYWORDs ronin_{load,resolve}_system`

        for key in ${RONIN_PARTs[@]} ${RONIN_LANGUAGEs[@]} ; do
            KEYWORDs=`echo $KEYWORDs ronin_{require,include,setup}_$key`
        done
        ;;
esac

#*******************************************************************************

for word in ${KEYWORDs[@]} ; do
    unset $word
done

unset word KEYWORDs

