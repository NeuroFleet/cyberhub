#!/bin/bash

export HOSTNAME=`hostname`

#*******************************************************************************

if [[ "x"$RONIN_DISK == "x" ]] ; then
    if [[ "x"$DISKTAPE != "x" ]] ; then
        if [[ -d /media/theque/$DISKTAPE ]] ; then
            export RONIN_DISK=/media/theque/$DISKTAPE
        fi
    fi

    if [[ ! -d $RONIN_DISK ]] ; then
        for hdd in SYSOP GHOST ; do
            for usr in $PERSONNA $IDENTITY $ORGANISM ; do
                if [[ -d /media/$usr/$hdd ]] ; then
                    export DISKTAPE=$hdd

                    export RONIN_DISK=/media/$usr/$hdd

                    export RONIN_HOME=$RONIN_DISK/home/$PERSONNA
                    export RONIN_DATA=$RONIN_DISK/opt/corp/$ORGANISM
                fi
            done
        done
    fi
fi

#*******************************************************************************

if [[ "x"$RONIN_ROOT == "x" ]] ; then
    if [[ "x"$RONIN_DISK == "x" ]] ; then
        export RONIN_ROOT=$PWD
    else
        export RONIN_ROOT=$RONIN_DISK/
    fi
fi

################################################################################

if [[ "x"$ORGANISM == "x" ]] ; then
    export ORGANISM=`hostname -d`

    if [[ "x"$ORGANISM == "x" ]] ; then
        export ORGANISM=`hostname -s`.ronin
    fi
fi

#*******************************************************************************

if [[ "x"$IDENTITY == "x" ]] ; then export IDENTITY=$USER ; fi

################################################################################

if [[ ! -d $RONIN_ROOT/sys ]] ; then
    for target in $DIR $FODLER $RONIN_DISK/hub $RONIN_DISK/psy $RONIN_DISK/app ; do
        if [[ -d $target/boot/kernel/spec.sh ]] ; then
            export RONIN_ROOT=$target
        fi
    done
fi

#*******************************************************************************

if [[ -f $RONIN_ROOT/boot/kernel/spec.sh ]] ; then
    source $RONIN_ROOT/boot/kernel/spec.sh
else
    echo "Unable to locate Ronin specs at : "$RONIN_ROOT

    exit
fi

################################################################################
#*******************************************************************************************************************************

MOTD_ROLE=console

#*******************************************************************************************************************************

source $RONIN_ROOT/boot/shell/environ.sh

#*******************************************************************************************************************************

source_deep $RONIN_BOOT/shell/{helper,tool}.d

#*******************************************************************************************************************************
################################################################################

#export RONIN_ROOT="/ron"
#export NOH_ROOT=$RONIN_ROOT"/usr"

#*******************************************************************************************************************************

#export RONIN_HOME_ROOT=/root
#export RONIN_HOME_DIR=$RONIN_HOME/home

#export RONIN_LANG_SUB=lib
#export RONIN_RECIPE=kyogen

#*******************************************************************************************************************************

#export RONIN_LANG_DIR=$RONIN_ROOT/$RONIN_LANG_SUB
#export RONIN_COOKBOOK=$NOH_ROOT/$RONIN_RECIPE

################################################################################################################################

#export CATALOGs=$RONIN_FILE/catalog

#*******************************************************************************

export RONIN_PATH=$RONIN_ROOT'/sbin:'$RONIN_PROG':'$RONIN_TOOL

################################################################################

#ensure_os_specs

#*******************************************************************************

#if [[ -d $RONIN_SPECS/tools ]] ; then
#    export RONIN_PATH=$RONIN_PATH":"$RONIN_SPECS/tools
#fi

