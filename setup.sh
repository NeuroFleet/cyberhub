#!/bin/bash

LOCALE=en_US.UTF-8

TARGET=/ronin

GIT_PREFIX=https://bitbucket.org

LANGUAGEs="scala go python coffee hack ruby swift"

#********************************************************************

PKGs="build-essential git unzip unrar-free"
PKGs=`echo $PKGs python-{virtualenv,pip,dev}`

#####################################################################

sudo localectl set-locale LANG=$LOCALE,LC_ALL=$LOCALE

locale-gen

#********************************************************************

apt update

apt -y upgrade

apt install -y --force-yes `echo $PKGs`

#********************************************************************

git clone $GIT_PREFIX/NeuroChip/cyberhub.git $TARGET --recursive

for key in $REALMs ; do
    git clone $GIT_PREFIX/IT-Bushido/$key.git $TARGET/lib/$key --recursive
done

#####################################################################

cd $TARGET

boot/strap build
boot/strap install

#********************************************************************


